package com.androidexample.totalsmscloneproject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SummaryAdpter extends ArrayAdapter<SummaryModel> {
    Context context;
    ArrayList<SummaryModel> data;

    private static final String LOG_TAG=SummaryAdpter.class.getSimpleName();
    public SummaryAdpter(Activity context, ArrayList<SummaryModel> data){
        super(context,0,data);
        this.context = context;
        this.data = data;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView=convertView;

        if(listItemView==null){
            listItemView=LayoutInflater.from(getContext()).inflate(R.layout.list_more,parent,false);
        }
        SummaryModel currentTotalSmsjava=getItem(position);
        TextView tv=(TextView) listItemView.findViewById(R.id.tv_a);
        tv.setText( currentTotalSmsjava.getMmobileno());

        TextView tv1=(TextView) listItemView.findViewById(R.id.tv_b);
        tv1.setText(currentTotalSmsjava.getMdate());

        TextView tv2=(TextView) listItemView.findViewById(R.id.tv_c);
        tv2.setText(currentTotalSmsjava.getMdeliverd());

        TextView tv3=(TextView) listItemView.findViewById(R.id.tv_d);
        tv3.setText(currentTotalSmsjava.getMtime());


        ImageView iconView = (ImageView) listItemView.findViewById(R.id.img_a);
        iconView.setImageResource(currentTotalSmsjava.getMcountryimage());
        return listItemView;


    }
}





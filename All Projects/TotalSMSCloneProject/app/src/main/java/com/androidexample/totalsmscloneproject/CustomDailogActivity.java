package com.androidexample.totalsmscloneproject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class CustomDailogActivity  extends Dialog implements
        android.view.View.OnClickListener  {

    TextView tv_clmoretab;
    TextView tv_smsmoretab;
    Maininterface maininterface;

    public Activity c;
    public Dialog d;


    public CustomDailogActivity(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_custom_dailog);
        maininterface = (Maininterface) c;

        tv_clmoretab= (TextView) findViewById(R.id.tv_clmoretab);
        tv_smsmoretab = (TextView) findViewById(R.id.tv_smsmoretab);
        tv_clmoretab.setOnClickListener(this);
        tv_smsmoretab.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_clmoretab:
             maininterface.Opencallsummary();
                dismiss();
                break;
            case R.id.tv_smsmoretab:
                maininterface.Opensmssummary();
                dismiss();
                break;
            default:
                break;
        }

    }
}
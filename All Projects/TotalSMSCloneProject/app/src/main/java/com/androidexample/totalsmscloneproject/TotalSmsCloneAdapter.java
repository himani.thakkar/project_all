package com.androidexample.totalsmscloneproject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TotalSmsCloneAdapter extends ArrayAdapter<TotalSmsCloneModel> {

    Context context;
    ArrayList<TotalSmsCloneModel> data;

    private static final String LOG_TAG=TotalSmsCloneAdapter.class.getSimpleName();
    public TotalSmsCloneAdapter(Activity context, ArrayList<TotalSmsCloneModel> data){
        super(context,0,data);
        this.context = context;
        this.data = data;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView=convertView;

        if(listItemView==null){
            listItemView=LayoutInflater.from(getContext()).inflate(R.layout.list_layout,parent,false);
        }
        TotalSmsCloneModel currentTotalSmsjava=getItem(position);
        TextView tv=(TextView) listItemView.findViewById(R.id.tv_menu);
        tv.setText( currentTotalSmsjava.getcountryname());

        TextView tv1=(TextView) listItemView.findViewById(R.id.tv_submenu);
        tv1.setText(currentTotalSmsjava.getcountrynumber());



        ImageView iconView = (ImageView) listItemView.findViewById(R.id.img_item);
        iconView.setImageResource(currentTotalSmsjava.getcountryimage());
        return listItemView;


    }
}



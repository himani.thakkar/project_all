package com.androidexample.totalsmscloneproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidexample.totalsmscloneproject.MainActivity;
import com.androidexample.totalsmscloneproject.R;
import com.androidexample.totalsmscloneproject.SummaryAdpter;
import com.androidexample.totalsmscloneproject.SummaryModel;
import com.androidexample.totalsmscloneproject.TotalSmsCloneAdapter;
import com.androidexample.totalsmscloneproject.TotalSmsCloneModel;

import java.util.ArrayList;


public class CallSummaryFragment extends BaseFragment {
    View v;

    public CallSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {
        ((MainActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getToolbarText().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getToolbarText().setText("Call Summary");
        ((MainActivity) getActivity()).getImg_back().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getImg_back().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainInterface.Openhome();
            }
        });
    }
    @Override
    public void setTabForFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_call_summary, container, false);

        final ArrayList<SummaryModel> words=new ArrayList<SummaryModel>();
        words.add(new SummaryModel("+8200342466","01-1-2019", "delivered","02.30PM",R.drawable.f1));
        words.add(new SummaryModel("+8978675423","03-1-2019","delivered","01.20PM",R.drawable.f2));
        words.add(new SummaryModel("+9876543456","01-1-2019","delivered","12.10AM",R.drawable.f3));
        words.add(new SummaryModel("+9988456787","31-12-2018","delivered","04.30PM",R.drawable.f4));
        words.add(new SummaryModel("+9088345678","04-1-2019","delivered","03.00AM",R.drawable.f5));
        SummaryAdpter adapter= new SummaryAdpter((MainActivity)getActivity(),words);
        ListView listView = (ListView) v.findViewById(R.id.ll_clsummary);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");

        return v;
    }

}

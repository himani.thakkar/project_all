package com.androidexample.totalsmscloneproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.androidexample.totalsmscloneproject.MainActivity;
import com.androidexample.totalsmscloneproject.R;
import com.androidexample.totalsmscloneproject.TotalSmsCloneAdapter;
import com.androidexample.totalsmscloneproject.TotalSmsCloneModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SMSFragment extends BaseFragment {
 View v;

    public SMSFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {
        ((MainActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getToolbarText().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getToolbarText().setText("SMS Test");
        ((MainActivity) getActivity()).getImg_back().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getImg_back().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainInterface.Openhome();


            }

        });
    }


    @Override
    public void setTabForFragment() {
        ((MainActivity) getActivity()).getLl_tab().setVisibility(View.VISIBLE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_sm, container, false);
        Spinner spinner = (Spinner) v.findViewById(R.id.sp_sms);
        ArrayAdapter<CharSequence> ad = ArrayAdapter.createFromResource((MainActivity)getActivity(), R.array.Country, android.R.layout.simple_spinner_item);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(ad);

        Spinner spinner2 = (Spinner) v.findViewById(R.id.sp_smstier);
        ArrayAdapter<CharSequence> asp = ArrayAdapter.createFromResource((MainActivity)getActivity(), R.array.Tier, android.R.layout.simple_spinner_item);
        asp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(asp);



        final ArrayList<TotalSmsCloneModel> words=new ArrayList<TotalSmsCloneModel>();
        words.add(new TotalSmsCloneModel("Belgium","+3214561235",R.drawable.f1));
        words.add(new TotalSmsCloneModel("Russia","+6128734568",R.drawable.f2));
        words.add(new TotalSmsCloneModel("Malawi","+7892345678",R.drawable.f3));
        words.add(new TotalSmsCloneModel("Nauru","+6785623789",R.drawable.f4));
        words.add(new TotalSmsCloneModel("Tonga","+2347845342",R.drawable.f5));
        TotalSmsCloneAdapter adapter= new TotalSmsCloneAdapter((MainActivity)getActivity(),words);
        ListView listView = (ListView) v.findViewById(R.id.ll_sms);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");


        return v;
    }

}

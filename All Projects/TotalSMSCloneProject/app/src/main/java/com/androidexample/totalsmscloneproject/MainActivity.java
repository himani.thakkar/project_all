package com.androidexample.totalsmscloneproject;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidexample.totalsmscloneproject.fragments.CallFragment;
import com.androidexample.totalsmscloneproject.fragments.CallSummaryFragment;
import com.androidexample.totalsmscloneproject.fragments.ConfigurationFragment;
import com.androidexample.totalsmscloneproject.fragments.HomeFragment;
import com.androidexample.totalsmscloneproject.fragments.SMSFragment;
import com.androidexample.totalsmscloneproject.fragments.SMSsummaryFragment;
import com.androidexample.totalsmscloneproject.fragments.SplashFragment;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public abstract class MainActivity extends AppCompatActivity implements Maininterface,View.OnClickListener {
    FrameLayout container;
    ImageView img_hometab;
    ImageView img_calltab;
    ImageView img_smstab;
    ImageView img_tab_config;
    ImageView img_moretab;
    ImageView img_back;
    TextView tv_toolbar;
    TextView tv_hometab;
    TextView tv_calltab;
    TextView tv_smstab;
    TextView tv_tabconfiguration;
    TextView tv_moretab;
    TextView tv_clmoretab;
    TextView tv_smsmoretab;
    LinearLayout ll_tab;
    LinearLayout ll_hometab;
    LinearLayout ll_calltab;
    LinearLayout ll_smstab;
    LinearLayout ll_tabconfiguration;
    LinearLayout ll_moretab;
//    LinearLayout ll_tabview;
//    LinearLayout ll_tab;


    private Toolbar toolbar;
    private TextView tab_label;
    private int[] tabIcons = {
            R.drawable.homegrey,
            R.drawable.callgrey,
            R.drawable.smsgrey,
            R.drawable.configurationgrey,
            R.drawable.moregrey
    };

    private int[] navLabels = {
            R.string.home,
            R.string.call,
            R.string.SMS,
            R.string.configuration,
            R.string.menu
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = findViewById(R.id.container);
        img_hometab = findViewById(R.id.img_hometab);
        img_calltab = findViewById(R.id.img_calltab);
        img_smstab = findViewById(R.id.img_smstab);
        img_moretab = findViewById(R.id.img_moretab);
        img_tab_config = findViewById(R.id.img_tabconfiguration);
        img_back=findViewById(R.id.img_back);
        ll_tab = findViewById(R.id.ll_tab);
        ll_hometab = findViewById(R.id.ll_hometab);
        ll_calltab = findViewById(R.id.ll_calltab);
        ll_smstab = findViewById(R.id.ll_smstab);
        ll_tabconfiguration = findViewById(R.id.ll_tabconfiguration);
//        ll_tabview=findViewById(R.id.ll_tabview);
        ll_moretab = findViewById(R.id.ll_moretab);
        tv_hometab = findViewById(R.id.tv_hometab);
        tv_calltab = findViewById(R.id.tv_calltab);
        tv_smstab = findViewById(R.id.tv_smstab);
        tv_tabconfiguration = findViewById(R.id.tv_tabconfiguration);
        tv_moretab = findViewById(R.id.tv_moretab);
        tv_toolbar=findViewById(R.id. tv_toolbar);
        tv_clmoretab=findViewById(R.id.tv_clmoretab);
        tv_smsmoretab=findViewById(R.id.tv_smsmoretab);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);


        clickListeners();

        }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_hometab:
                Openhome();
                break;
            case R.id.ll_calltab:
                Opencall();
                break;
            case R.id.ll_smstab:
                Opensms();
                break;
            case R.id.ll_tabconfiguration:
                Openconfiguration();
                break;
            case R.id.ll_moretab:
                Openmore();
                break;
        }
    }

    private void clickListeners() {
        ll_hometab.setOnClickListener(this);
        ll_calltab.setOnClickListener(this);
        ll_smstab.setOnClickListener(this);
        ll_tabconfiguration.setOnClickListener(this);
        ll_moretab.setOnClickListener(this);
        img_back.setOnClickListener(this);


    }
//        @Override
//        public void Openhome () {
//            container.setVisibility(View.VISIBLE);
//            replaceFragment(new HomeFragment(),R.id.container,HomeFragment.class.getSimpleName(),false);
//            }
//
//
//
//            public void Opencall(){
//           container.setVisibility(View.VISIBLE);
//            }


    @Override
    public void Opensplash() {
//        toolbar.setVisibility(View.GONE);
//        container.setVisibility(View.VISIBLE);
//        ll_tab.setVisibility(View.GONE);
        replaceFragment(new SplashFragment(), R.id.container, SplashFragment.class.getSimpleName(), false);


    }

    @Override
    public void Opencall() {
        Onselectedcall();
        Onunselectedotherthancall();
//        container.setVisibility(View.VISIBLE);
//        toolbar.setVisibility(View.VISIBLE);
        replaceFragment(new CallFragment(), R.id.container, CallFragment.class.getSimpleName(), false);

    }

    @Override
    public void Openhome() {
        Onselectedhome();
//        ll_tab.setVisibility(View.VISIBLE);
        Onunselectedotherthanhome();
//        toolbar.setVisibility(View.GONE);
//        container.setVisibility(View.VISIBLE);
        replaceFragment(new HomeFragment(), R.id.container, HomeFragment.class.getSimpleName(), false);
    }

    @Override
    public void Opensms() {
        Onselectedsms();
        Onunselectedotherthansms();
//        toolbar.setVisibility(View.VISIBLE);
//        container.setVisibility(View.VISIBLE);
        replaceFragment(new SMSFragment(), R.id.container, SMSFragment.class.getSimpleName(), false);

    }

    @Override
    public void Openconfiguration() {
        Onselectedconfiguration();
        Onunselectedotherthanconfiguration();
//        toolbar.setVisibility(View.VISIBLE);
//        container.setVisibility(View.VISIBLE);
        replaceFragment(new ConfigurationFragment(), R.id.container, ConfigurationFragment.class.getSimpleName(), false);

    }

    @Override
    public void Openmore() {
        Onselectedmore();
        Onunselectedotherthanmore();
        CustomDailogActivity cdd=new CustomDailogActivity(this);
        cdd.show();
//        toolbar.setVisibility(View.VISIBLE);
//        container.setVisibility(View.VISIBLE);


    }
    @Override
    public void Opencallsummary(){
//        toolbar.setVisibility(View.VISIBLE);
//       container.setVisibility(View.VISIBLE);
        replaceFragment(new CallSummaryFragment(), R.id.container, CallSummaryFragment.class.getSimpleName(), false);


    }
    @Override
    public void Opensmssummary(){
//        toolbar.setVisibility(View.VISIBLE);
//      container.setVisibility(View.VISIBLE);
        replaceFragment(new SMSsummaryFragment(), R.id.container, SMSsummaryFragment.class.getSimpleName(), false);

    }



//    @Override
//  public void Opendailog(){
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//// Add the buttons
//        builder.setPositiveButton(R.string.CallTestSumarry, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                // User clicked OK button
//            }
//        });
//        builder.setNegativeButton(R.string.SMSTestSumarry, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                // User cancelled the dialog
//            }
//        });
//        AlertDialog dialog = builder.create()
//
//    }



    public void replaceFragment(Fragment mFragment, int id, String tag, boolean addToStack) {
        FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
        mTransaction.replace(id, mFragment);
//        hideKeyboard();
        if (addToStack) {
            mTransaction.addToBackStack(tag);
        }
        mTransaction.commitAllowingStateLoss();
    }

    //set & get for hometab

    public void setImg_hometab(ImageView img_hometab) {
        this.img_hometab = img_hometab;

    }

    public ImageView getImg_hometab() {
        return img_hometab;
    }

    public void setTv_hometab(TextView tv_hometab) {
        this.tv_hometab = tv_hometab;

    }

    public TextView getTv_hometab() {
        return tv_hometab;
    }

//set & get for call tab

    public void setTv_calltab(TextView tv_calltab) {
        this.tv_calltab = tv_calltab;

    }

    public TextView getTv_calltab() {
        return tv_calltab;
    }


    public void setImg_calltab(ImageView img_calltab) {
        this.img_calltab = img_calltab;

    }

    public ImageView getImg_calltab() {
        return img_calltab;
    }

//set & get for smstab

    public void setImg_smstab(ImageView img_smstab) {
        this.img_smstab = img_smstab;

    }

    public ImageView getImg_smstab() {
        return img_smstab;
    }

    public void setTv_smstab(TextView tv_smstab) {
        this.tv_smstab = tv_smstab; }

    public TextView getTv_smstab() { return tv_smstab; }

    //set & get for configuration

    public void setImg_tab_config(ImageView img_tab_config) {
        this.img_tab_config = img_tab_config; }

    public ImageView getImg_tab_config() { return img_tab_config; }

    public void setTv_tabconfigurationtab(TextView tv_tabconfiguration){
        this.tv_tabconfiguration = tv_tabconfiguration; }
        public TextView getTv_tabconfiguration(){ return tv_tabconfiguration; }


        //set & get for moretab

    public void setImg_moretab(ImageView img_moretab){
     this.img_moretab = img_moretab; }

    public ImageView getImg_moretab(){ return img_moretab; }

    public void setTv_moretab(TextView tv_moretab){
        this.tv_moretab = tv_moretab; }

    public TextView getTv_moretab(){ return tv_moretab; }

        public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar; }
        public Toolbar getToolbar(){
        return toolbar; }

        public void setLinerLayout(LinearLayout ll_tab) {
        this.ll_tab = ll_tab; }

    public void setToolbarText(TextView tv_toolbar) {
        this.tv_toolbar = tv_toolbar; }

    public TextView getToolbarText(){
        return tv_toolbar; }

        public void setImg_back(ImageView img_back) {
        this.img_back = img_back;
    }
    public ImageView getImg_back(){
        return img_back; }


    public LinearLayout getLl_tab(){
        return ll_tab; }

    public void Onselectedcall(){
        img_calltab.setImageResource(R.drawable.callblue);
        tv_calltab.setTextColor(getResources().getColor(R.color.selectedtext));
        }

    public void Onunselectedotherthancall(){
        img_hometab.setImageResource(R.drawable.homegrey);
        img_smstab.setImageResource(R.drawable.smsgrey);
        img_tab_config.setImageResource(R.drawable.configurationgrey);
        img_moretab.setImageResource(R.drawable.moregrey);
        tv_hometab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_smstab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_tabconfiguration.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_moretab.setTextColor(getResources().getColor(R.color.unselecttext));


    }
    public void Onselectedhome(){
        img_hometab.setImageResource(R.drawable.homeblue);
        tv_hometab.setTextColor(getResources().getColor(R.color.selectedtext));

    }

    public void Onunselectedotherthanhome(){
        img_calltab.setImageResource(R.drawable.callgrey);
        img_smstab.setImageResource(R.drawable.smsgrey);
        img_tab_config.setImageResource(R.drawable.configurationgrey);
        img_moretab.setImageResource(R.drawable.moregrey);
        tv_calltab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_smstab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_tabconfiguration.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_moretab.setTextColor(getResources().getColor(R.color.unselecttext));


    }
    public void Onselectedsms(){
        img_smstab.setImageResource(R.drawable.smsblue);
        tv_smstab.setTextColor(getResources().getColor(R.color.selectedtext));
       }

    public void Onunselectedotherthansms(){
        img_hometab.setImageResource(R.drawable.homegrey);
        img_calltab.setImageResource(R.drawable.callgrey);
        img_tab_config.setImageResource(R.drawable.configurationgrey);
        img_moretab.setImageResource(R.drawable.moregrey);
        tv_hometab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_calltab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_tabconfiguration.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_moretab.setTextColor(getResources().getColor(R.color.unselecttext));



    }

    public void Onselectedconfiguration(){
        img_tab_config.setImageResource(R.drawable.configurationblue);
        tv_tabconfiguration.setTextColor(getResources().getColor(R.color.selectedtext));}

    public void Onunselectedotherthanconfiguration(){
        img_hometab.setImageResource(R.drawable.homegrey);
        img_calltab.setImageResource(R.drawable.callgrey);
        img_smstab.setImageResource(R.drawable.smsgrey);
        img_moretab.setImageResource(R.drawable.moregrey);
        tv_hometab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_calltab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_smstab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_moretab.setTextColor(getResources().getColor(R.color.unselecttext));


    }

    public void Onselectedmore(){
        img_moretab.setImageResource(R.drawable.moreblue);
        tv_moretab.setTextColor(getResources().getColor(R.color.selectedtext));}

    public void Onunselectedotherthanmore(){
        img_hometab.setImageResource(R.drawable.homegrey);
        img_calltab.setImageResource(R.drawable.callgrey);
        img_smstab.setImageResource(R.drawable.smsgrey);
        img_tab_config.setImageResource(R.drawable.configurationgrey);
        tv_hometab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_calltab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_smstab.setTextColor(getResources().getColor(R.color.unselecttext));
        tv_tabconfiguration.setTextColor(getResources().getColor(R.color.unselecttext));



    }
}



package com.androidexample.totalsmscloneproject;

public class TotalSmsCloneModel {
    private String mcountryname;
    private String mcountrynumber;
    private int mcountryimage;

    public TotalSmsCloneModel(String countryname, String countrynumber, int countryimage) {
        this.mcountryname = countryname;
        this.mcountrynumber = countrynumber;
        this.mcountryimage = countryimage;
    }

    public String getcountryname(){
        return mcountryname;
    }

    public  String getcountrynumber(){
        return mcountrynumber;
    }
    public int getcountryimage(){
        return  mcountryimage;
    }
}

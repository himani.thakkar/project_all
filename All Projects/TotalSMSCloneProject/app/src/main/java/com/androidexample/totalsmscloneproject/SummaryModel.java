package com.androidexample.totalsmscloneproject;

public class SummaryModel {
    private String mmobileno;
    private String mdate;
    private String mdeliverd;
    private String mtime;
    private int mcountryimage;

    public SummaryModel(String mmobileno, String mdate,  String mdeliverd,String mtime, int mcountryimage) {
        this.mmobileno = mmobileno;
        this.mdate = mdate;
        this.mdeliverd = mdeliverd;
        this.mtime = mtime;
        this.mcountryimage = mcountryimage;
    }

    public String getMmobileno() {
        return mmobileno;
    }

    public void setMmobileno(String mmobileno) {
        this.mmobileno = mmobileno;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getMtime() {
        return mtime;
    }

    public void setMtime(String mtime) {
        this.mtime = mtime;
    }

    public String getMdeliverd() {
        return mdeliverd;
    }

    public void setMdeliverd(String mdeliverd) {
        this.mdeliverd = mdeliverd;
    }

    public int getMcountryimage() {
        return mcountryimage;
    }

    public void setMcountryimage(int mcountryimage) {
        this.mcountryimage = mcountryimage;
    }
}




package com.androidexample.totalsmscloneproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.androidexample.totalsmscloneproject.MainActivity;
import com.androidexample.totalsmscloneproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {
    View v;
    LinearLayout ll_tc;
    LinearLayout ll_ts;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {
        ((MainActivity) getActivity()).getToolbar().setVisibility(View.GONE);

    }

    @Override
    public void setTabForFragment() {
        ((MainActivity) getActivity()).getLl_tab().setVisibility(View.VISIBLE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_home, container, false);

        // Inflate the layout for this fragment

        ll_tc = v.findViewById(R.id.ll_tc);
        ll_tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.Opencall();
            }
        });
        ll_ts = v.findViewById(R.id.ll_ts);
        ll_ts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.Opensms();
            }
        });
        return v;

    }

}

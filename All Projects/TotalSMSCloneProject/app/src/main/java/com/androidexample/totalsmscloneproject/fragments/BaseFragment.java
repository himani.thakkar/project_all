package com.androidexample.totalsmscloneproject.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidexample.totalsmscloneproject.MainActivity;
import com.androidexample.totalsmscloneproject.Maininterface;
import com.androidexample.totalsmscloneproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {

    public abstract void setToolbarForFragment();
    public abstract void setTabForFragment();
    Maininterface mainInterface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainInterface = (Maininterface) getActivity();
        setTabForFragment();
        setToolbarForFragment();
    }
}
package com.androidexample.totalsmscloneproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.androidexample.totalsmscloneproject.MainActivity;
import com.androidexample.totalsmscloneproject.R;
import com.androidexample.totalsmscloneproject.TotalSmsCloneAdapter;
import com.androidexample.totalsmscloneproject.TotalSmsCloneModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigurationFragment extends BaseFragment {
View v;

    public ConfigurationFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {
        ((MainActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getToolbarText().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getToolbarText().setText("Configuration");
        ((MainActivity) getActivity()).getImg_back().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getImg_back().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainInterface.Openhome();
                }
                });
    }

    @Override
    public void setTabForFragment() {
        ((MainActivity) getActivity()).getLl_tab().setVisibility(View.VISIBLE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_configuration, container, false);
        Spinner spinner = (Spinner) v.findViewById(R.id.configuration_spinner);
        ArrayAdapter<CharSequence> ad = ArrayAdapter.createFromResource((MainActivity) getActivity(), R.array.choosecalltime, android.R.layout.simple_spinner_item);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(ad);



        Spinner spinner2 = (Spinner) v.findViewById(R.id.configuration_spinner2);
        ArrayAdapter<CharSequence> adp = ArrayAdapter.createFromResource((MainActivity) getActivity(), R.array.numberofsms, android.R.layout.simple_spinner_item);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adp);
        return v;

    }

}

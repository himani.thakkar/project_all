package com.example.asplphp4.myrecyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RecyclerAdpter extends RecyclerView.Adapter<RecyclerAdpter.ImageViewHolder> {

    private int [] images;
    Context context;

    public RecyclerAdpter(Context context,int[]images2){
        this.context=context;
        this.images=images2;
    }




    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_layout,parent,false);
        ImageViewHolder imageViewHolder=new ImageViewHolder(view);
        return imageViewHolder;
    }
//    private OnItemClicked onClick;
    //make interface like this
//    public interface OnItemClicked {
//        void onItemClick(int position);
//    }
//
//    public void setOnClick(OnItemClicked onClick)
//    {
//        this.onClick=onClick;
//    }

    @Override
    public void onBindViewHolder(@NonNull  ImageViewHolder holder,final int position) {
        int img_id=images[position];
        holder.img.setImageResource(img_id);
        holder.tv.setText("Images:"+position);

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onClick.onItemClick(position);
                Toast.makeText(context, "Images:"+position, Toast.LENGTH_SHORT).show();


            }
        });
        }
    @Override
    public int getItemCount() {
        return images.length;
    }
    public static class ImageViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView tv;
        public  ImageViewHolder (View itemView){
            super(itemView);

            tv=itemView.findViewById(R.id.tv_main);
        }



    }
}

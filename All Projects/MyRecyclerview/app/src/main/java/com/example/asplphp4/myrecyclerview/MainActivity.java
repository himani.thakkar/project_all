package com.example.asplphp4.myrecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  {
    ImageView img_main;
    private RecyclerView recyclerView;
    private int []images={R.drawable.blackimg,R.drawable.brownimg,R.drawable.brownimg,R.drawable.blackimg,R.drawable.blackimg,R.drawable.brownimg,R.drawable.brownimg,R.drawable.blackimg};
    private RecyclerAdpter adpter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.rv_main);
        layoutManager =new GridLayoutManager(this,2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        adpter=new RecyclerAdpter(getApplicationContext(),images);
        recyclerView.setAdapter(adpter);
//        adpter.setOnClick(MainActivity.this);


//    }
//
//    @Override
//    public void onItemClick(int position) {
        // The onClick implementation of the RecyclerView item click
        //ur intent code here


    }
}

package com.androidexample.creatlist;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CreateActivity extends AppCompatActivity {
    ImageView i1;
    TextView t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        i1=(ImageView)findViewById(R.id.img_first);
        t=(TextView)findViewById(R.id.tv_hello) ;
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            int resId=bundle.getInt("resId");
            String message=bundle.getString("message");
            t.setText(message);
            i1.setImageResource(resId);
        }


    }

}
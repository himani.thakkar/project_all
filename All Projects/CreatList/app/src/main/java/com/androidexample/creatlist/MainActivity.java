package com.androidexample.creatlist;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {
    ListView lv_first;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<CreateModel> al = new ArrayList<CreateModel>();
        al.add(new CreateModel("one", R.drawable.a1));
        al.add(new CreateModel("two", R.drawable.a1));
        al.add(new CreateModel("three", R.drawable.a1));
        al.add(new CreateModel("four", R.drawable.a1));
        al.add(new CreateModel("five", R.drawable.a1));
        al.add(new CreateModel("six", R.drawable.a1));
        al.add(new CreateModel("seven", R.drawable.a1));
        al.add(new CreateModel("eight", R.drawable.a1));
        al.add(new CreateModel("night", R.drawable.a1));
        al.add(new CreateModel("ten", R.drawable.a1));


        CreateAdapter adapter = new CreateAdapter(this, al);
        ListView listView = (ListView) findViewById(R.id.lv_first);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                CreateModel cm = al.get(position);
                Intent intent = new Intent(MainActivity.this, CreateActivity.class);

                 intent.putExtra("message",cm.getlistmenu());
                intent.putExtra( "resId",cm.getimageid());
                startActivity(intent);
            }
        });
    }
}

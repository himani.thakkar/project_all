package com.androidexample.loginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText edt_name;
    EditText edt_pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     edt_name = (EditText) findViewById(R.id.edt_name);
     edt_pass=(EditText)findViewById( R.id.edt_pass);
    }


    public void onButtonClick(View view) {

        String str = edt_name.getText().toString();
        String str1 = edt_pass.getText().toString();

        if (!str.equalsIgnoreCase("") && (!str1.equalsIgnoreCase(""))) {
            Intent i = new Intent(MainActivity.this, Display.class);
            i.putExtra("username", str);
            startActivity(i);
        } else

        {
            if (edt_name.getText().toString().trim().equalsIgnoreCase("")) {
                edt_name.setError("This field can not be blank");
            }
            else {
             if  (edt_pass.getText().toString().trim().equalsIgnoreCase("")) {
                    edt_pass.setError("This field can not be blank");

                }
            }
        }

    }


}
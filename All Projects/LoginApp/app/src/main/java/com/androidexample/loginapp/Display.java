package com.androidexample.loginapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Display extends AppCompatActivity {
    String username = "";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        textView = findViewById(R.id.textView);
        Bundle extras;
        extras = getIntent().getExtras();
        if (extras == null) {
            username = null;
        } else {
            username = getIntent().getStringExtra("username");
        }

        textView.setText("Welcome "+username);
    }
}
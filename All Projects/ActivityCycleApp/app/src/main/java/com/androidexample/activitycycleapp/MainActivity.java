package com.androidexample.activitycycleapp;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

    public class MainActivity extends AppCompatActivity {
        TextView TextView;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
             TextView = findViewById(R.id.view);
            tToast("onCreate");
        }
        @Override
        protected void onStart() {
            super.onStart();
            tToast("onStart");
        }

        @Override
        protected void onResume() {
            super.onResume();
            tToast("onResume");
        }
        @Override
        protected void onPause() {
            super.onPause();
            tToast("onPause");

        }
        @Override
        protected void onStop() {
            super.onStop();
            tToast("onStop");

        }
        @Override
        protected void onRestart() {
            super.onRestart();
            tToast("onRestart");

        }
        @Override
        protected void onDestroy() {
            super.onDestroy();
            tToast("onDestroy.");
        }
        private void tToast(String s) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, s, duration);
            toast.show();
        }



        /**
         * This method is called when the order button is clicked.
         */
        public void potato(View view) {
            String str="Poteto";
            display(str);

        }


        public void tomato(View view) {
            String str="Tamato";
            displaytomato(str);

        }

        public void lf(View view){
            String str="LadiesFinger";
            display(str);

        }

        public void Onion(View view){
            String str="onion";
            display(str);

        }

        public void CF(View view){
            String str="cauliflower";
            display(str);

        }
        public void carrot(View view){
            String str="carrot";
            display(str);

        }
        public void biber(View view){
            String str="Biber";
            display(str);

        }
        public void cabbage(View view){
            String str="Cabbage";
            display(str);

        }
        public void beet(View view){
            String str="Beet";
            display(str);

        }
        public void cucumber(View view){
            String str="Cucumber";
            display(str);

        }
        public void GP(View view){
            String str="GreenPeas";
            display(str);

        }


        public void EP(View view){
            String str="EggPlant";
            display(str);

        }


        public void remove(View view){
         String str=" ";
            display(str);


        }


        /**
         * This method displays the given quantity value on the screen.
         */
        private void display(String str) {

            TextView.setText(str);
        }

        private void displaytomato(String str) {

            TextView.setText(str);
        }


        /* private void displayPrice(int number) {
            TextView PriTextView = (TextView) findViewById(R.id.Price_text_view);
            PriTextView.setText(NumberFormat.getCurrencyInstance().format(number));
        }
        */
    }


package com.androidexample.totalsmsdemo.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.R;

public class ConfigurationFragment extends BaseFragment {
    View v;

    public ConfigurationFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {

    }

    @Override
    public void setTabForFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        v = inflater.inflate(R.layout.fragment_configuration, container, false);
        ((MainActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
//        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Configuration");


        Spinner spinner = (Spinner) v.findViewById(R.id.configuration_spinner);
        ArrayAdapter<CharSequence> ad = ArrayAdapter.createFromResource((MainActivity) getActivity(), R.array.choosecalltime, android.R.layout.simple_spinner_item);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(ad);



        Spinner spinner2 = (Spinner) v.findViewById(R.id.configuration_spinner2);
        ArrayAdapter<CharSequence> adp = ArrayAdapter.createFromResource((MainActivity) getActivity(), R.array.numberofsms, android.R.layout.simple_spinner_item);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adp);
        return v;

    }

}
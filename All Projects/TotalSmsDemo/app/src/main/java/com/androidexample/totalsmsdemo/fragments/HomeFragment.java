package com.androidexample.totalsmsdemo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {


    public HomeFragment() {
        // Required empty public constructor
    }

    View v;
    LinearLayout ll_tc;
    LinearLayout ll_ts;


    @Override
    public void setToolbarForFragment() {
        ((MainActivity)getActivity()).getToolbar().setVisibility(View.GONE);
        ((MainActivity)getActivity()).getToolbarImage().setVisibility(View.GONE);
        ((MainActivity)getActivity()).getToolbarText().setVisibility(View.GONE);

    }

    @Override
    public void setTabForFragment() {
        ((MainActivity)getActivity()).getTabLayout().setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        getSupportActionBar().hide();

        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home, container, false);
//        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Home");
        ll_tc = v.findViewById(R.id.ll_tc);
        ll_tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.Opencall();
            }
        });
        ll_ts = v.findViewById(R.id.ll_ts);
        ll_ts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.Opensms();
            }
        });
        return v;
    }

}

package com.androidexample.totalsmsdemo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.R;
import com.androidexample.totalsmsdemo.TotalSmsAdapter;
import com.androidexample.totalsmsdemo.TotalSmsModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SMScallSummaryFragment extends BaseFragment {
    View v;


    public SMScallSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {
        ((MainActivity)getActivity()).getToolbar().setVisibility(View.VISIBLE);

        ((MainActivity)getActivity()).getToolbarText().setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).getToolbarText().setText("SMS Test Summary");
        ((MainActivity)getActivity()).getToolbarImage().setVisibility(View.VISIBLE);

    }

    @Override
    public void setTabForFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate(R.layout.fragment_smscall_summary, container, false);


        final ArrayList<TotalSmsModel> words=new ArrayList<TotalSmsModel>();
        words.add(new TotalSmsModel("Belgium","+321675490876",R.drawable.f1));
        words.add(new TotalSmsModel("Russia","+612345678097",R.drawable.f1));
        words.add(new TotalSmsModel("Malawi","+789876545234",R.drawable.f1));
        words.add(new TotalSmsModel("Nauru","+678945537908",R.drawable.f1));
        words.add(new TotalSmsModel("Tonga","+234123467098",R.drawable.f1));
        TotalSmsAdapter adapter= new TotalSmsAdapter((MainActivity)getActivity(),words);
        ListView listView = (ListView) v.findViewById(R.id.ll_call);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");




        return v;
    }

}

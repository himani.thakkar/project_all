package com.androidexample.totalsmsdemo.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toolbar;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.MainInterface;
import com.androidexample.totalsmsdemo.R;
import com.androidexample.totalsmsdemo.TotalSmsAdapter;
import com.androidexample.totalsmsdemo.TotalSmsModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallFragment extends BaseFragment {

    View v;
    Toolbar toolbar;


    public CallFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {
        ((MainActivity)getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).getToolbarText().setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).getToolbarText().setText("Call Test");
        ((MainActivity)getActivity()).getToolbarImage().setVisibility(View.VISIBLE);

//        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.id.rl_tb);

    }

    @Override
    public void setTabForFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_call, container, false);




        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> ad = ArrayAdapter.createFromResource((MainActivity)getActivity(), R.array.Country, android.R.layout.simple_spinner_item);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(ad);

        Spinner spinner2 = (Spinner) v.findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> asp = ArrayAdapter.createFromResource((MainActivity)getActivity(), R.array.Tier, android.R.layout.simple_spinner_item);
        asp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(asp);



        final ArrayList<TotalSmsModel> words=new ArrayList<TotalSmsModel>();
        words.add(new TotalSmsModel("Belgium","+321675490876",R.drawable.f1));
        words.add(new TotalSmsModel("Russia","+612345678097",R.drawable.f1));
        words.add(new TotalSmsModel("Malawi","+789876545234",R.drawable.f1));
        words.add(new TotalSmsModel("Nauru","+678945537908",R.drawable.f1));
        words.add(new TotalSmsModel("Tonga","+234123467098",R.drawable.f1));
      TotalSmsAdapter adapter= new TotalSmsAdapter((MainActivity)getActivity(),words);
        ListView listView = (ListView) v.findViewById(R.id.ll_call);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");



    return v;
    }

}


package com.androidexample.totalsmsdemo;

public class TotalSmsModel {
    private String mcountryname;
    private String mcountrynumber;
    private int mcountryimage;

    public TotalSmsModel(String countryname, String countrynumber, int countryimage) {
        this.mcountryname = countryname;
        this.mcountrynumber = countrynumber;
        this.mcountryimage = countryimage;
    }

    public String getcountryname(){
        return mcountryname;
    }

    public  String getcountrynumber(){
        return mcountrynumber;
    }
    public int getcountryimage(){
        return  mcountryimage;
    }
}

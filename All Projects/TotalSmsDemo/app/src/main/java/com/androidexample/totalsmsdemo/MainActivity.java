package com.androidexample.totalsmsdemo;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidexample.totalsmsdemo.fragments.CallFragment;
import com.androidexample.totalsmsdemo.fragments.CallTestSummaryFragment;
import com.androidexample.totalsmsdemo.fragments.ConfigurationFragment;
import com.androidexample.totalsmsdemo.fragments.HomeFragment;
import com.androidexample.totalsmsdemo.fragments.SMSFragment;
import com.androidexample.totalsmsdemo.fragments.SMScallSummaryFragment;
import com.androidexample.totalsmsdemo.fragments.SplashFragment;

import java.util.ArrayList;
import java.util.List;

public abstract class MainActivity extends AppCompatActivity implements MainInterface {
     private Button button;
    private Toolbar toolbar;
    private ImageView tab_icon;
    private TabLayout tabLayout;
    private CustomViewpager viewPager;
    ImageView img_cl;
    ImageView img_sms;
    ImageView img_tb;
    TextView tv_tb;

    private TextView tab_label;
    private int[] tabIcons = {
            R.drawable.homemain,
            R.drawable.cllogo,
            R.drawable.msglogo,
            R.drawable.settinglogo,
            R.drawable.more
    };

    private int[] navLabels = {
            R.string.home,
            R.string.call,
            R.string.SMS,
            R.string.Setting,
            R.string.menu
    };
    private int[] navIconsActive = {
            R.drawable.selectorhome,
            R.drawable.selector_call,
            R.drawable.selector_sms,
            R.drawable.selector_setting,
            R.drawable.selector_menu
    };

    FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        img_tb=(ImageView) findViewById(R.id.img_tb);
        tv_tb=(TextView) findViewById(R.id.tv_tb);

        viewPager = (CustomViewpager) findViewById(R.id.viewpager);
        container = findViewById(R.id.container);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        viewPager = (CustomViewpager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setEnableSwipe(false);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setTabIcons();
//        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
//        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
//        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
//        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
//        tabLayout.getTabAt(4).setIcon(tabIcons[4]);

    }

//    button.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            viewPager.setEnableSwipe(false);
//        }
//    });


    private void setTabIcons() {
// loop through all navigation tabs
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            LinearLayout tab = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);

            // get child TextView and ImageView from this layout for the icon and label
            TextView tab_label = (TextView) tab.findViewById(R.id.nav_label);
            ImageView tab_icon = (ImageView) tab.findViewById(R.id.nav_icon);

            // set the label text by getting the actual string value by its id
            // by getting the actual resource value `getResources().getString(string_id)`
            tab_label.setText(getResources().getString(navLabels[i]));
            tab_icon.setImageResource(navIconsActive[i]);
            // set the home to be active at first
            if (i == 0) {
//                tab_label.setTextColor(getResources().getColor(R.color.colorAccent));
//                tab_icon.setImageResource(navIconsActive[i]);
                tab_icon.setSelected(true);
            } else {
                tab_icon.setSelected(false);
            }

            // finally publish this custom view to navigation tab
            tabLayout.getTabAt(i).setCustomView(tab);
        }
    }




    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(new CallFragment(), "Call");
        adapter.addFragment(new SMSFragment(), "SMS");
        adapter.addFragment(new ConfigurationFragment(), "Configuration");
        viewPager.setAdapter(adapter);
    }
    @Override
    public void Openhome() {
        container.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        viewPager.setCurrentItem(0,false);
    }

    @Override
    public void Opencall() {
        container.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
   viewPager.setCurrentItem(1,false);
    }

    @Override
    public void Opensms() {
        container.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        viewPager.setCurrentItem(2,false);

    }

    @Override
    public void OpenConfiguration() {
        container.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
  viewPager.setCurrentItem(3,false);
    }

    @Override
    public void Openmenu() {
        container.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        viewPager.setCurrentItem(4,false);

    }

    @Override
    public void Opencalltestsummary(){
        container.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        replaceFragment(new CallTestSummaryFragment(),R.id.container,CallTestSummaryFragment.class.getSimpleName(),false);

        }

    @Override
    public void OpenSmstestsummary(){
        container.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        replaceFragment(new SMScallSummaryFragment(),R.id.container,SMScallSummaryFragment.class.getSimpleName(),false);



    }

    @Override
    public void Opensplash() {
        container.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        replaceFragment(new SplashFragment(),R.id.container,SplashFragment.class.getSimpleName(),false);
    }





    public void replaceFragment(Fragment mFragment, int id, String tag, boolean addToStack) {
        FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
        mTransaction.replace(id, mFragment);
//        hideKeyboard();
        if (addToStack) {
            mTransaction.addToBackStack(tag);
        }
        mTransaction.commitAllowingStateLoss();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }


    public void setTabLayout(TabLayout tabLayout) {
        this.tabLayout = tabLayout;
    }
   public void setToolbarText(TextView tv_tb) {
        this.tv_tb = tv_tb;
    }
    public void setToolbarImage(ImageView img_tb){
        this.img_tb=img_tb;
    }
    public Toolbar getToolbar(){
        return toolbar;
    }

   public TextView getToolbarText(){
        return tv_tb;
    }
    public ImageView getToolbarImage(){
        return img_tb;
    }
    public TabLayout getTabLayout(){
        tabLayout.setupWithViewPager(viewPager);
        setTabIcons();
        return tabLayout;
    }







}


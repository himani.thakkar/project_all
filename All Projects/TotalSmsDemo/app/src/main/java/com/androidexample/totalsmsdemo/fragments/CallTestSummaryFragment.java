package com.androidexample.totalsmsdemo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.R;
import com.androidexample.totalsmsdemo.TotalSmsAdapter;
import com.androidexample.totalsmsdemo.TotalSmsModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallTestSummaryFragment extends BaseFragment {
    View v;

    public CallTestSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {


        ((MainActivity)getActivity()).getToolbar().setVisibility(View.VISIBLE);

        ((MainActivity)getActivity()).getToolbarText().setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).getToolbarText().setText("Call Test Summary");
        ((MainActivity)getActivity()).getToolbarImage().setVisibility(View.VISIBLE);

    }

    @Override
    public void setTabForFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v=inflater.inflate(R.layout.fragment_call_test_summary, container, false);


        return v;
    }

}

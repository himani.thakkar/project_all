package com.androidexample.incrementdecriment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

      int num=2;
    public void increment(View view) {
        num=num+1;
        display(num);

    }

      public void decriment(View view) {
        num=num-1;
        display(num);

    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        int Price=num*10;
       String msg="Total"+num+"" + "\n Thank you..";
       displayMsg("msg");

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * This method displays the given quantity value on the screen.
     */
       private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }


    private void displayPrice(int number) {
        TextView PriTextView = (TextView) findViewById(R.id.Price_text_view);
        PriTextView.setText(NumberFormat.getCurrencyInstance().format(number));
    }

    private  void displayMsg(String s){

    }

}


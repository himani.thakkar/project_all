package com.androidexample.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_one;
    Button btn_two;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        OnClickListener();
    }
  public void initView(){
        btn_one=findViewById(R.id.btn_one);
        btn_two=findViewById(R.id.btn_two);
  }

  public void OnClickListener(){
        btn_one.setOnClickListener(this);
        btn_two.setOnClickListener(this);
  }

    @Override
    public void onClick(View v) {
         switch (v.getId()){

             case R.id.btn_one:
                 Intent i1=new Intent(this,OneActivity.class);
                 startActivity(i1);
                 break;

             case R.id.btn_two:
                 Intent i2=new Intent(this,TwoActivity.class);
                 startActivity(i2);
                 break;
         }

    }
}

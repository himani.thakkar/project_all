package com.androidexample.sortingapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class JavaAdapter extends ArrayAdapter<Modelclass> {
    private static final String LOG_TAG = JavaAdapter.class.getSimpleName();

    public JavaAdapter(Activity context, ArrayList<Modelclass>calllist ) {
        super(context, 0,calllist );
    }
    @Override
      public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;

        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        Modelclass m = getItem(position);
        TextView tv_country=(TextView) listItemView.findViewById(R.id.tv_country);
        TextView tv_phno=(TextView) listItemView.findViewById(R.id.tv_phno);
        tv_country.setText(m.getCountry());
        tv_phno.setText(m.getPhoneNumber());
        return listItemView;
    }
}
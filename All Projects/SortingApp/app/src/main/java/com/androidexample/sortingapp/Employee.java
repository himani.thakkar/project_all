package com.androidexample.sortingapp;


public class Employee implements Comparable<Employee> {
    private String name;
    private String id;
    private double salary;

    public Employee(String id, String name, double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getSalary() {

        return salary;
    }

    public void setSalary(double salary) {

        this.salary = salary;
    }

    @Override
    public int compareTo(Employee employee) {
        double compareSalary = ((Employee) employee).getSalary();

        // ascending order
        // return (int) (this.salary - compareSalary);

        // descending order
        return (int) (compareSalary - this.salary);
    }

    @Override
    public String toString() {
        return "[ id=" + id + ", name=" + name + ", salary=" + salary + "]";
    }

}
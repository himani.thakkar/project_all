package com.androidexample.cateringapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class PanjabiThaliActivity extends AppCompatActivity {
    android.support.v7.widget.Toolbar tl;
    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.PanjabiMenu);
        setContentView(R.layout.activity_panjabi_thali);

        tl = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tl);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        layout = (LinearLayout) findViewById(R.id.pt);




        final ArrayList<WordModel> words=new ArrayList<WordModel>();
        words.add(new WordModel("Kaju Mutter Masala","Kaju,Panner,Butter",R.drawable.pn1));
        words.add(new WordModel("Paneer Tikka.","Tamatar,Paneer,Green Mirchi",R.drawable.pn5));
        words.add(new WordModel("Peshawari Paneer.","Milk,Paneer,Badam,Keasar",R.drawable.pn2));
        words.add(new WordModel("Sarson ka Saag","Ginger,Kanda,Green Mirchi,Tamatar ",R.drawable.pn3));
        words.add(new WordModel("Chole Tikki Chaat","Chole,Green-Mirchi,Dahi,Sev,Chatni",R.drawable.pn4));



        WordAdapter adapter=new WordAdapter(this,words);
        ListView listView = (ListView) findViewById(R.id.lv_panj);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                WordModel wm1 = words.get(position);
                Intent intent = new Intent(PanjabiThaliActivity.this, PanjabiPrice.class);

                intent.putExtra("message",wm1.getlistmenu());
                intent.putExtra( "resId",wm1.getimgid());
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}

package com.androidexample.cateringapp;

public class WordModel {
    private String mlistmenu;
    private String mlistsubmenu;
    private int mimgid=NO_IMAGE_PROVIDED;
    private static final int NO_IMAGE_PROVIDED = -1;


    public WordModel(String listmenu,String listsubmenu){
        mlistmenu=listmenu;
        mlistsubmenu=listsubmenu;
    }



    public WordModel(String listmenu, String listsubmenu, int imgid){
        mlistmenu=listmenu;
        mlistsubmenu=listsubmenu;
        mimgid=imgid;
    }


    public  String getlistmenu(){

        return  mlistmenu;
    }

    public String getlistsubmenu(){
        return mlistsubmenu;

    }
    public  int getimgid(){

        return  mimgid;
    }

    public boolean hasImage() {
        return mimgid != NO_IMAGE_PROVIDED;
    }


}
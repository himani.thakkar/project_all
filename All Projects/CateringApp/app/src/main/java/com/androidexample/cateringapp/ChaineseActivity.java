package com.androidexample.cateringapp;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class ChaineseActivity extends AppCompatActivity {
    android.support.v7.widget.Toolbar tl;
    LinearLayout layout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.ChaineseMenu);
        setContentView(R.layout.activity_chainese);
        tl = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tl);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        layout = (LinearLayout) findViewById(R.id.ll_chn);


        final ArrayList<WordModel> words=new ArrayList<WordModel>();
        words.add(new WordModel("Sweet and Sour Pork","Corn,Tamater,Green Sauce,Red Chilly Sauce",R.drawable.ch1));
        words.add(new WordModel("Ma Po Tofu ","Noodles,Chilly Sauce,Ginger",R.drawable.ch2));
        words.add(new WordModel("Spring Rolls","Round Rolls ,Noodles,Chilly Sauce",R.drawable.ch4));
        words.add(new WordModel("Quick Noodles","Noodles,Chilly,Tamater",R.drawable.ch1));
        words.add(new WordModel(" Hot and Sour Soup","Gajar,Kanda,Lasun,Spring Onion",R.drawable.ch));



        WordAdapter adapter=new WordAdapter(this,words);
        ListView listView = (ListView) findViewById(R.id.lv_chn);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                WordModel wm1 = words.get(position);
                Intent intent = new Intent(ChaineseActivity.this, ChainesePrice.class);
                intent.putExtra("message",wm1.getlistmenu());
                intent.putExtra( "resId",wm1.getimgid());
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

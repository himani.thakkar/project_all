package com.androidexample.cateringapp;

public class WordJava {
    private String mlistmenu;
    private String mlistsubmenu;
    private int mimgid;


    public WordJava(String listmenu, String listsubmenu,int imgid){
        mlistmenu=listmenu;
        mlistsubmenu=listsubmenu;
        mimgid=imgid;
    }


    public  String getlistmenu(){
        return  mlistmenu;
    }

    public String getlistsubmenu(){
        return mlistsubmenu;

    }
    public  int getimgid(){
        return  mimgid;
    }

}
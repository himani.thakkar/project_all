package com.androidexample.cateringapp;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SauthIn extends AppCompatActivity {
    TextView txt;

    android.support.v7.widget.Toolbar tl;
    LinearLayout layout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.SauthIndianMenu);
        setContentView(R.layout.activity_sauth_in);

        tl = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tl);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        layout = (LinearLayout) findViewById(R.id.ll_sin);


        txt = (TextView) findViewById(R.id.txt_sp);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int resId = bundle.getInt("resId");
            String message = bundle.getString("message");
            txt.setText(message);
        }
    }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            switch (item.getItemId()) {
                case android.R.id.home:
                    onBackPressed();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
    }
package com.androidexample.cateringapp;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class SauthIndianActivity extends AppCompatActivity {
    android.support.v7.widget.Toolbar tl;
    LinearLayout layout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.SauthIndianMenu);
        setContentView(R.layout.activity_sauth_indian);


        tl = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tl);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        layout = (LinearLayout) findViewById(R.id.ll_sin);



        final ArrayList<WordModel> words=new ArrayList<WordModel>();
        words.add(new WordModel(" Masala Dosa","Soak Rice,Aalu,Tamatar,Green Chilly"));
        words.add(new WordModel("Idli","Spicy Dal,Pices of Idli"));
        words.add(new WordModel("MenduWada","Spicy Dal, Soak Rice,Pices of Mendu Wada"));
        words.add(new WordModel("Hyderabadi Biryani","Rice,Tamatar,Aalu"));
        words.add(new WordModel("Paper Dosa","Plain Dosa"));


        WordAdapter adapter=new WordAdapter(this,words);
        ListView listView = (ListView) findViewById(R.id.lv_sin);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                WordModel wm = words.get(position);
                Intent intent = new Intent(SauthIndianActivity.this, SauthIn.class);

                intent.putExtra("message",wm.getlistmenu());
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

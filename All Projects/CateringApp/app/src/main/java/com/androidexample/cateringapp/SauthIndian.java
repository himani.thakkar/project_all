package com.androidexample.cateringapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class SauthIndian extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sauth_indian);
        ArrayList<WordJava> words=new ArrayList<WordJava>();
        words.add(new WordJava("Bhindi","fry bhindi,tamatar,green mirchi",R.drawable.g1));
        words.add(new WordJava("Methi Papad","methi,papad,green mirchi",R.drawable.g1));
        words.add(new WordJava("Oondhiya","tamatar,bateka,kanda",R.drawable.g1));
        words.add(new WordJava("Sev Tameta","sev,tamatar,green mirchi",R.drawable.g1));
        words.add(new WordJava("Raitu","Dahi,green-mirchi",R.drawable.g1));



        WordAdapter adapter=new WordAdapter(this,words);
        ListView listView = (ListView) findViewById(R.id.lv_sin);
        listView.setAdapter(adapter);
        Log.i("myInfoTag","infomsg");
    }
}

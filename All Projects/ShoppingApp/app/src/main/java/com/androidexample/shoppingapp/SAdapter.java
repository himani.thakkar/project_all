package com.androidexample.shoppingapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SAdapter extends ArrayAdapter<Word> {


    private static final String LOG_TAG=SAdapter.class.getSimpleName();
    public SAdapter(Activity context, ArrayList<Word>words){
        super(context,0,words);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView=convertView;
        if(listItemView==null){
            listItemView=LayoutInflater.from(getContext()).inflate(R.layout.list_menu,parent,false);
        }
        Word currentword=getItem(position);
        TextView tv=(TextView) listItemView.findViewById(R.id.tv_menu);
        tv.setText(currentword.getlistmenu());

        TextView tv1=(TextView) listItemView.findViewById(R.id.tv_submenu);
        tv1.setText(currentword.getlistsubmenu());



        ImageView iv = (ImageView) listItemView.findViewById(R.id.img_cl);
        iv.setImageResource(currentword.getimgid());


        return listItemView;


    }
}
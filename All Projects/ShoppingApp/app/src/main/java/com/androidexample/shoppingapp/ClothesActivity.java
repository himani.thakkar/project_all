package com.androidexample.shoppingapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class ClothesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clothes);

        ArrayList<Word> words=new ArrayList<Word>();
        words.add(new Word("Tops","Rs:1000",R.drawable.cl1));
        words.add(new Word("Jeans","Rs.1500",R.drawable.cl1));
        words.add(new Word("Skirt","Rs.1000",R.drawable.cl1));
        words.add(new Word("Shorts","Rs.1700",R.drawable.cl1));
        words.add(new Word("Jeckets","Rs.2000",R.drawable.cl1));
        SAdapter ad=new SAdapter(this,words);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(ad);
        Log.i("myInfoTag","infomsg");



    }

}



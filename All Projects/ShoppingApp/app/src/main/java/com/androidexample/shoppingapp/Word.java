package com.androidexample.shoppingapp;

public class Word {

    private String mlistmenu;
    private String mlistsubmenu;
    private int mimgid;


       public Word(String listmenu, String listsubmenu, int imgid) {
        mlistmenu = listmenu;
        mlistsubmenu = listsubmenu;
        mimgid = imgid;
    }
    public Word(String listmenu, String listsubmenu) {
        mlistmenu = listmenu;
        mlistsubmenu = listsubmenu;

    }


    public String getlistmenu() {

        return mlistmenu;
    }

    public String getlistsubmenu() {
        return mlistsubmenu;

    }

    public int getimgid() {

        return mimgid;
    }
}